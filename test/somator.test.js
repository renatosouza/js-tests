const somator = require("../src/somator");

describe("somator tests", () => {
    test("should sum all numbers of a given array", () => {
        const nums = [1, 2, 3, 7, 8];

        const result = nums.reduce((prev, actual) => somator(prev)(actual));

        expect(result).toBe(21);
    });
    test("should sum 2 for each number of a given array", () => {
        const nums = [1, 2, 3, 4, 5];
        const expectedResult = [3, 4, 5, 6, 7];

        const result = nums.map((number) => somator(2)(number));

        expect(result).toEqual(expectedResult);
    });
});
