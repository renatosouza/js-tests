const { sortAlphabetically, toList } = require("../src/lists");
const { readFileSync } = require("fs");
const path = require("path");

describe("Testando listas simples", () => {
  let lista = new Array();
  const arr = ["1", "2", "3"];

  beforeAll(() => {
    lista.push(1, 2, 3, 4, 5);
  });

  test("A lista deve ter 5 itens", () => {
    expect(lista.length).toBe(5);
  });

  test("deve inserir um item na lista", () => {
    lista.push(6);
    expect(lista.length).toBe(6);
  });

  test("deve remover o último item da lista", () => {
    expect(lista.splice(-1)[0]).toBe(6);
  });
});

describe("Testando ordenação em listas", () => {
  test("deve retornar exception para string vazia.", () => {
    expect(() => sortAlphabetically("")).toThrow("Empty String!");
  });
  test("deve ordenar alfabeticamente as letras da palavra passada.", () => {
    expect(sortAlphabetically("badc")).toEqual("abcd");
  });
});

describe("organizando dados com Arrays", () => {
  let csv = "";
  beforeAll(() => {
    csv = readFileSync(
      path.resolve(__dirname, "./fixtures/test_list.csv")
    ).toString();
  });
  test("deve capturar itens de uma tabela e montar uma matriz", () => {
    const lines = toList(csv, "\n");
    const headers = toList(lines[0]);

    lines.shift();

    const dicts = lines.map((line) => {
      const columns = toList(line);
      let item = {};

      headers.forEach((header, index) => {
        item = { ...item, [header]: columns[index] };
      });
      return item;
    });

    expect(Object.keys(dicts[0])).toEqual(headers);
    expect(dicts[0].nome).toBe("Renato");
  });
});
