const { Carro, Pessoa, executaAcao, Produto } = require("../src/objects");
describe("Teste de objetos", function () {
  test("test", () => {
    const carros = new Array();

    const maverick = new Carro("Maverick", "Ford", "Branco");
    const mustang = new Carro("Mustang Gt Chelby", "Ford", "Vermelho");

    carros.push(maverick, mustang);

    const pessoas = [];

    const Diego = new Pessoa("Diego", 28, "M");
    const Renato = new Pessoa("Renato", 32, "M");

    pessoas.push(Diego, Renato);

    expect(executaAcao("cumprimentar", Diego, "Renato")).toBe("Olá Renato");
    expect(executaAcao("cumprimentar", Diego, "Renato")).toBe("Olá Renato");
  });
});

describe("teste Livros", () => {
  test("criando lista de livros", function () {
    const livro = new Produto(
      "Os dois morrem no final",
      "https://images-submarino.b2w.io/produtos/3535073901/imagens/livro-os-dois-morrem-no-final/3535073901_1_medium.jpg",
      6,
      43.89,
      2,
      ["ficção", "novela", "Suspense"]
    );
    const livro1 = new Produto(
      "Os dois morrem no Começo",
      "https://images-submarino.b2w.io/produtos/3535073901/imagens/livro-os-dois-morrem-no-final/3535073901_1_medium.jpg",
      2,
      20.5,
      10,
      ["Ação", "Suspense"]
    );

    const listaLivros = [livro, livro1];
    expect(listaLivros[1].getParcelamento()).toBe("10x de R$ 2.05");

    const listaRoupas = [
      new Produto("Camisa Masculina gola V", "imagem da camisa", 2, 59.9, 10),
      new Produto(
        "Camisa Feminina gola V",
        "imagem da camisa Feminina",
        5,
        79.9,
        6
      ),
    ];

    expect(listaRoupas[0].getParcelamento()).toBe("10x de R$ 5.99");
  });
});
