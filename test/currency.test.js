const { currencyConverter } = require("../src/conversor");

describe("currency conversor tests", () => {
    test("should convert BRL to USD", () => {
        const factor = 4.8;
        const expectedResult = 9.6;

        const dolarConverter = currencyConverter(factor);

        expect(dolarConverter(2)).toEqual(expectedResult);
    });

    test("should convert USD to BRL", () => {
        const factor = 0.21;
        const expectedResult = 0.84;

        const realFromDolar = currencyConverter(factor);

        expect(realFromDolar(4)).toEqual(expectedResult);
    });
});
