describe("context tests", () => {
    test("should change variable of correct context", () => {
        let a = "c";

        {
            let a = "a";
            a = "b";
            expect(a).toBe("b");
        }

        expect(a).toBe("c");
    });

    test("should access the correct variable from context", () => {
        let a = "c";

        {
            let a = "a";
            expect(a).toBe("a");
        }

        expect(a).toBe("c");
    });
});
