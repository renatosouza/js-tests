const { converter } = require("../src/conversor");
const multiply = require("../src/multiply");
const somator = require("../src/somator");

describe("currying tests", () => {
    test("should return function to double numbers", () => {
        const double = multiply(2);

        expect(double(1.5)).toBe(3);
        expect(double(2)).toBe(4);
        expect(double(10)).toBe(20);
    });

    test("should return function to triple numbers", () => {
        const triple = multiply(3);

        expect(triple(1.5)).toBe(4.5);
        expect(triple(2)).toBe(6);
        expect(triple(10)).toBe(30);
    });

    test("should return function to triple N numbers", () => {
        const numbers = [1.5, 2, 10];
        const expectedResult = [4.5, 6, 30];
        const multiplyN = (nums) => nums.map(multiply(3));

        expect(multiplyN(numbers)).toEqual(expectedResult);
    });

    test("should return function to convert celsius to farenheit", () => {
        const celsiusToFarenheit = converter(1.8)(32);

        expect(celsiusToFarenheit(25)).toBe(77);
    });

    test("should sum each number of given array with its own triple", () => {
        const numbers = [1, 2, 3];
        const expectedResult = [4, 8, 12];

        const result = numbers.map((number) => somator(multiply(3)(number))(number));

        expect(result).toEqual(expectedResult);
    });
});
