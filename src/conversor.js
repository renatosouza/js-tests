const multiply = require("./multiply");

module.exports = {
    converter(a) {
        return function (b) {
            return function (x) {
                return multiply(a)(x) + b;
            };
        };
    },
    currencyConverter(factor) {
        return multiply(factor);
    }
};
