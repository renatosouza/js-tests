module.exports = function multiply(multiplicand) {
    return function (multiplicator) {
        return multiplicand * multiplicator;
    };
};
