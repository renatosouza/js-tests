module.exports = {
  sortAlphabetically(text = "") {
    if (text.length > 0) {
      return text
        .split("")
        .sort(
          (a, b) =>
            text.charCodeAt(text.indexOf(a)) - text.charCodeAt(text.indexOf(b))
        )
        .join("");
    }

    throw new Error("Empty String!");
  },
  toList(string, separator = ",") {
    return string.split(separator);
  },
};
