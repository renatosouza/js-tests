class Pessoa {
  constructor(nome, idade, genero) {
    this.nome = nome;
    this.idade = idade;
    this.genero = genero;
  }

  cumprimentar(nomePessoa) {
    return "Olá " + nomePessoa;
  }
}

class Carro {
  constructor(nome, marca, cor) {
    this.nome = nome;
    this.marca = marca;
    this.cor = cor;
  }

  acelerar(limiteVelocidade) {
    return "Acelerando a " + limiteVelocidade;
  }
}

const executaAcao = function (action, object, argumento) {
  return object[action](argumento);
};

class Produto {
  constructor(nome, imagem, nota, preco, parcelamento, categorias) {
    this.nome = nome;
    this.imagem = imagem;
    this.nota = nota;
    this.preco = preco;
    this.parcelamento = parcelamento;
    this.categorias = categorias;
  }

  getParcelamento() {
    const resultadoParcelamento = (this.preco / this.parcelamento).toFixed(2);
    return this.parcelamento + "x de R$ " + resultadoParcelamento;
  }
}

module.exports = {
  Pessoa,
  Carro,
  executaAcao,
  Produto,
};
