module.exports = function somator(a) {
    return function (b) {
        return a + b;
    };
};
